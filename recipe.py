import shutil
from os.path import dirname, join

from packagetools.builder import *
from packagetools.command import cygwin_path, run

VERSION = "llvmorg-15.0.4"
RECIPE_DIR = dirname(__file__)


class Recipe(PackageShortRecipe):
    def describe(self) -> PackageInfo:
        return PackageInfo(
            description="Build LLVM libcxx",
            id="llvm_libcxx",
            pretty_name="LLVM libcxx",
            version=VERSION,
        )

    def all_steps(self, out_dir: str):
        bash = shutil.which("bash")
        out_dir = cygwin_path(out_dir)
        recipe = cygwin_path(join(RECIPE_DIR, "recipe.sh"))
        run(bash, recipe, VERSION, out_dir)


PackageBuilder(Recipe()).make()

#!/usr/bin/env bash

set -euo pipefail
set -x

script_dir=$(dirname $(readlink -f $0))

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

set -euo pipefail

checkout()
{
    if [ -d llvm ]; then return; fi

    local version=$1
    git clone https://gitlab.com/Linaro/windowsonarm/forks/llvm
    pushd llvm
    git checkout $version
    git log -n1
    popd
}

build()
{
    pushd llvm
    clang-cl.exe --version
    python=$(which python)
    [ "$python" != "" ] || die "can't find python in PATH"
    python_dir=$(dirname $python)
    python_dir=$(cygpath -w "$python_dir")

    # https://libcxx.llvm.org/BuildingLibcxx.html

    # we need to build with clang-cl
    export CC=clang-cl.exe CXX=clang-cl.exe

    #cmake -G Ninja -S runtimes -B build -DLLVM_ENABLE_RUNTIMES="libcxx;libcxxabi;libunwind"
    #cmake -G Ninja -S runtimes -B build -DLLVM_ENABLE_RUNTIMES="libcxx;libcxxabi" \
    #    -DLIBCXX_ENABLE_SHARED=YES \
    #   -DLIBCXXABI_ENABLE_STATIC=NO

    # libcxxabi can't be built with clang-cl for now. And it does not seem that
    # it's needed.
    #
    # We need to build in Release or else, some test triggers
    # "Microsoft Visual C++ Runtime Error" dialog, that can't be removed by any
    # setting, except, by recompiling program. So, it hangs those tests.
    # https://stackoverflow.com/questions/55423522/microsoft-visual-studio-c-runtime-disable-debug-assertion-from-console
    # https://stackoverflow.com/questions/1938715/disable-microsoft-visual-c-runtime-error
    cmake -G Ninja -S runtimes -B build \
        -DLLVM_ENABLE_RUNTIMES="libcxx" -DPython3_ROOT_DIR="${python_dir}" \
        -DCMAKE_BUILD_TYPE=Release
    ninja -C build all
    popd
}

run_tests()
{
    pushd llvm

    # except failures
    ninja -C build check-cxx || true

    # to run test manually
    #cd build/libcxx/test
    #python ../../bin/llvm-lit.py ../../libcxx/test/ -s -v
}

checkout $version
build
run_tests
